import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
    const currentUser = ref<User>({
    id: 1,
    email: 'Hina123@gmail.com',
    password: 'Pass@1334',
    fullName: 'ฮานะ ฮิยะ',
    gender: 'female',
    roles: ['user']
})
  return { currentUser }
})
