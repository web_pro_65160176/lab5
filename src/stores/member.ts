import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'


export const useMemberStore = defineStore('member', () => {
    const members = ref<Member[]>([
        { id:1, name: 'สมชาย รักแม่', tel:'0946485931'},
        { id:2, name: 'สมหญิง รักแม่', tel:'0986485931'}
    ])
    const currentMember = ref<Member | null >()
    const searchMember = (tel: string)=> {
        const index = members.value.findIndex((item) => item.tel === tel)
        if (index < 0) {
            currentMember.value = null
        }
        currentMember.value = members.value[index]
    }
    function clear() {
        currentMember.value = null
    }
    return { members,currentMember,
    searchMember , clear
}
})
