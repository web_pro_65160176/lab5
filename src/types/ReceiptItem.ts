import type { Product } from "./Product";

type ReceipItem = {
    id: number
    name: string
    price: number
    unit: number
    productsId: number
    product?: Product
}

export { type ReceipItem}